-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : lucasjxtestcdsi.mysql.db
-- Généré le : lun. 20 nov. 2023 à 09:00
-- Version du serveur : 5.7.42-log
-- Version de PHP : 8.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `lucasjxtestcdsi`
--
CREATE DATABASE IF NOT EXISTS `lucasjxtestcdsi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `lucasjxtestcdsi`;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `identifiant` int(10) UNSIGNED NOT NULL,
  `etat_commande` enum('panier','validée','prête','collectée') NOT NULL DEFAULT 'panier',
  `identifiant_coordonnees_client` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `coordonnees_client`
--

CREATE TABLE `coordonnees_client` (
  `identifiant` int(10) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telephone` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `information`
--

CREATE TABLE `information` (
  `identifiant` int(11) NOT NULL,
  `adresse` varchar(100) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `horaire` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `information`
--

INSERT INTO `information` (`identifiant`, `adresse`, `telephone`, `horaire`) VALUES
(1, '12 rue des Peupliers, 35150 Janzé', '0678901234', 'Du lundi au vendredi, de 9h30 à 12h30 et de 14h à 18h');

-- --------------------------------------------------------

--
-- Structure de la table `ligne_commande`
--

CREATE TABLE `ligne_commande` (
  `identifiant_produit` int(10) UNSIGNED NOT NULL,
  `identifiant_commande` int(10) UNSIGNED NOT NULL,
  `quantite` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `identifiant` int(10) UNSIGNED NOT NULL,
  `nom` varchar(100) CHARACTER SET utf8 NOT NULL,
  `image` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT 'placeholder',
  `prix_au_kg` float NOT NULL,
  `disponible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`identifiant`, `nom`, `image`, `prix_au_kg`, `disponible`) VALUES
(1, 'Carotte', '/imgs/products/carotte.png', 1.1, 1),
(2, 'Endive', '/imgs/products/endive-belge', 3, 1),
(3, 'Haricots verts', '/imgs/products/haricots-verts.png', 1, 1),
(4, 'Citrouille', '/imgs/products/citrouille.png', 0.9, 0),
(5, 'Radis', '/imgs/products/radis.png', 1.5, 1),
(6, 'Pomme de terre', '/imgs/products/pommes-de-terre.png', 2, 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`identifiant`),
  ADD KEY `Commande_coordonnees_client_identifiant_fk` (`identifiant_coordonnees_client`);

--
-- Index pour la table `coordonnees_client`
--
ALTER TABLE `coordonnees_client`
  ADD PRIMARY KEY (`identifiant`);

--
-- Index pour la table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`identifiant`);

--
-- Index pour la table `ligne_commande`
--
ALTER TABLE `ligne_commande`
  ADD PRIMARY KEY (`identifiant_produit`,`identifiant_commande`),
  ADD KEY `ligne_commande_commande_identifiant_fk` (`identifiant_commande`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`identifiant`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `identifiant` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `coordonnees_client`
--
ALTER TABLE `coordonnees_client`
  MODIFY `identifiant` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `information`
--
ALTER TABLE `information`
  MODIFY `identifiant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `identifiant` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `Commande_coordonnees_client_identifiant_fk` FOREIGN KEY (`identifiant_coordonnees_client`) REFERENCES `coordonnees_client` (`identifiant`);

--
-- Contraintes pour la table `ligne_commande`
--
ALTER TABLE `ligne_commande`
  ADD CONSTRAINT `ligne_commande_commande_identifiant_fk` FOREIGN KEY (`identifiant_commande`) REFERENCES `commande` (`identifiant`) ON DELETE CASCADE,
  ADD CONSTRAINT `ligne_commande_produit_identifiant_fk` FOREIGN KEY (`identifiant_produit`) REFERENCES `produit` (`identifiant`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
