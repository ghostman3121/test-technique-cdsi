# Test technique CDSI

## Prérequis :
- Un serveur MySQL en fonctionnement et un SGBD
- Une installation de PHP 8.2.12
- L'installation de Composer
- L'installation de Symfony CLI (préférablement via Scoop)
- L'installation de Git

## Installation :
- Importez du dépôt sur votre machine dans le dossier via la commande ```git clone <URL du dépôt>```
- Attention à ce que la variable path menant à PHP mène à la bonne version
- Importez à l'aide de votre SGBD le script lucasjxtestcdsi_mysql_db.sql
- Spécifiez dans le fichier /src/Repository/ConnectionDB.php les informations nécessaires à la connexion à votre serveur MySQL
- Exécutez la commande ``Symfony server:start`` à la racine du projet à l'aide d'un terminal
