<?php

namespace App\Entity;

class Order
{
    #region Attributes
    private int $id;
    private array $order_lines;
    private CustomerDetails $customer_details;
    private OrderState $order_state;
    #endregion

    /**
     * @param int $id
     * @param array $order_lines
     * @param CustomerDetails $customer_details
     * @param OrderState $order_state
     */
    public function __construct(int $id, array $order_lines, CustomerDetails $customer_details, OrderState $order_state)
    {
        $this->id = $id;
        $this->order_lines = $order_lines;
        $this->customer_details = $customer_details;
        $this->order_state = $order_state;
    }

    #region Getters and Setters
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getOrderLines(): array
    {
        return $this->order_lines;
    }

    /**
     * @param array $order_lines
     */
    public function setOrderLines(array $order_lines): void
    {
        $this->order_lines = $order_lines;
    }

    /**
     * @return CustomerDetails
     */
    public function getCustomerDetails(): CustomerDetails
    {
        return $this->customer_details;
    }

    /**
     * @param CustomerDetails $customer_details
     */
    public function setCustomerDetails(CustomerDetails $customer_details): void
    {
        $this->customer_details = $customer_details;
    }

    /**
     * @return OrderState
     */
    public function getOrderState(): OrderState
    {
        return $this->order_state;
    }

    /**
     * @param OrderState $order_state
     */
    public function setOrderState(OrderState $order_state): void
    {
        $this->order_state = $order_state;
    }
    #endregion
}