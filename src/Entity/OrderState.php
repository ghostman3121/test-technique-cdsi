<?php

namespace App\Entity;

enum OrderState
{
    case Basket;
    case Validated;
    case Ready;
    case Collected;
}