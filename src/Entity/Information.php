<?php

namespace App\Entity;

class Information
{
    #region Attributes
    private int $id;
    private string $address;
    private string $phone_number;
    private string $schedule;
    #endregion

    /**
     * @param int $id
     * @param string $address
     * @param string $phone_number
     * @param string $schedule
     */
    public function __construct(int $id, string $address, string $phone_number, string $schedule)
    {
        $this->id = $id;
        $this->address = $address;
        $this->phone_number = $phone_number;
        $this->schedule = $schedule;
    }

    #region Getters and Setters
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
     /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phone_number;
    }

    /**
     * @param string $phone_number
     */
    public function setPhoneNumber(string $phone_number): void
    {
        $this->phone_number = $phone_number;
    }

    /**
     * @return string
     */
    public function getSchedule(): string
    {
        return $this->schedule;
    }

    /**
     * @param string $schedule
     */
    public function setSchedule(string $schedule): void
    {
        $this->schedule = $schedule;
    }
    #endregion
}