<?php

namespace App\Entity;

class OrderLine
{
    #region Attributes
    private int $id;
    private Product $product;
    private float $quantity;
    private Order $order;
    #endregion

    /**
     * @param Product $product
     * @param float $quantity
     * @param Order $order
     * @param int $id
     */
    public function __construct(int $id, Product $product, float $quantity, Order $order)
    {
        $this->id = $id;
        $this->product = $product;
        $this->quantity = $quantity;
        $this->order = $order;
    }

    #region Getters and Setters
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }
    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return float
     */
    public function getQuantity(): float
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity(float $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }
    #endregion
}