<?php

namespace App\Entity;

class Product
{
    #region Attributes
    private int $id;
    private string $name;
    private string $image;
    private float $price_per_kilo;
    private bool $available;
    #endregion

    /**
     * @param string $name
     * @param string $image
     * @param float $price_per_kilo
     * @param bool $available
     */
    public function __construct(int $id,string $name, string $image, float $price_per_kilo, bool $available)
    {
        $this->id = $id;
        $this->name = $name;
        $this->image = $image;
        $this->price_per_kilo = $price_per_kilo;
        $this->available = $available;
    }



    #region Getters and Setters
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return float
     */
    public function getPricePerKilo(): float
    {
        return $this->price_per_kilo;
    }

    /**
     * @param float $price_per_kilo
     */
    public function setPricePerKilo(float $price_per_kilo): void
    {
        $this->price_per_kilo = $price_per_kilo;
    }

    /**
     * @return bool
     */
    public function isAvailable(): bool
    {
        return $this->available;
    }

    /**
     * @param bool $available
     */
    public function setAvailable(bool $available): void
    {
        $this->available = $available;
    }
    #endregion
}