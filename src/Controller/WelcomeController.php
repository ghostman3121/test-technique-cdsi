<?php

namespace App\Controller;

use App\Repository\InformationPDO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WelcomeController extends AbstractController
{
    #[Route('/', name:'welcome')]
    public function show(): Response
    {
        $information = InformationPDO::select_from_id(1);
        return $this->render('main/welcome.html.twig', [
            'address' => $information->getAddress(),
            'phone' => rtrim(chunk_split($information->getPhoneNumber(),2, '.'),'.'),
            'schedule' => $information->getSchedule()
        ]);
    }
}