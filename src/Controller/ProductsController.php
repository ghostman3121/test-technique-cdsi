<?php

namespace App\Controller;

use App\Repository\ProductPDO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductsController extends AbstractController
{
        #[Route('/liste-produits', name:'products')]
    public function show(): Response
    {
        return $this->render('main/products.html.twig', [
            'products' => ProductPDO::list()
        ]);
    }
}