<?php

namespace App\Repository;

use Exception;
use PDO;

class ConnectionDB
{
    private static $instance;
    //Singleton
    private function __construct()
    {
    }
    //Singleton
    private function __clone()
    {
    }
    public static function getInstance()
    {
        if (!isset($instance)) {
            try {
                $server_name = "localhost";
                $user = "root";
                $password = "root";
                $database_name = "test_technique_cdsi";
                $instance = new PDO(
                    'mysql:host=' . $server_name .
                        ';dbname=' . $database_name .
                        ';charset=utf8',
                    $user,
                    $password
                );
            } catch (Exception $e) {
                die('Erreur : ' . $e->getMessage());
            }
        }
        return $instance;
    }
}