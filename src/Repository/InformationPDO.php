<?php

namespace App\Repository;

use App\Entity\Information;

class InformationPDO
{
    public static function select_from_id($id): Information
    {
        $toReturn = null;
        $information = CRUD::Select('information', array('identifiant' => $id));
        if (isset($information[0])) {
            $information = $information[0];
            $toReturn = new Information(
                $id,
                $information['adresse'],
                $information['telephone'],
                $information['horaire']
            );
        }
        return $toReturn;
    }
    public static function modify(Information $information): int
    {
        return CRUD::Update(
            'information',
            array(
                'nom' => $information->getAddress(),
                'image' => $information->getPhoneNumber(),
                'prix_au_kilo' => $information->getSchedule(),
            ),
            array('id' => $information->getId())
        );
    }

}