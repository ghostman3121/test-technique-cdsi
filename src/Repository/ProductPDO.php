<?php

namespace App\Repository;

use App\Entity\Product;

class ProductPDO
{
    public static function select_from_id($id): Product
    {
        $toReturn = null;
        $product = CRUD::Select('produit', array('identifiant' => $id));
        if (isset($product[0])) {
            $product = $product[0];
            $toReturn = new Product(
                $id,
                $product['nom'],
                $product['image'],
                $product['prix_au_kg'],
                $product['disponible']
            );
        }
        return $toReturn;
    }
    public static function list(): array
    {
        $products_db = CRUD::Select('produit');
        $list_products = [];
        foreach ($products_db as $key => $product) {
            if (isset($product)) {
                $list_products[] = new Product(
                    $product['identifiant'],
                    $product['nom'],
                    $product['image'],
                    $product['prix_au_kg'],
                    $product['disponible']
                );
            }
        }
        return $list_products;
    }
    public static function create(Product $product): int
    {
        return CRUD::Insert(
            'produit',
            array(
                'nom' => $product->getName(),
                'image' => $product->getImage(),
                'prix_au_kg' => $product->getPricePerKilo(),
                'disponible' => $product->isAvailable()
            )
        );
    }
    public static function modify(Product $product): int
    {
        return CRUD::Update(
            'produit',
            array(
                'nom' => $product->getName(),
                'image' => $product->getImage(),
                'prix_au_kg' => $product->getPricePerKilo(),
                'disponible' => $product->isAvailable()
            ),
            array('identifiant' => $product->getId())
        );
    }
    public static function supress(Product $product): int
    {
        return CRUD::Delete('product', array('id' => $product->getId()));
    }
}